# bus_cat

## Pinout

### SPI bus
| Signal| Pin |
|-----| ---|
| SCK  | PA5 |
| MISO | PA6 |
| MOSI | PA7 |

### GPIO outputs 

| GPIO | Direction | 
|---  | --- |
| PA0| OUTPUT|
| PA1| OUTPUT|
| PA2| OUTPUT|
| PA3| OUTPUT|
| PA4| OUTPUT|
| PC13 (LED)| OUTPUT|
| PC14 | OUTPUT|
