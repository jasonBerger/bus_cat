/**
  *@file app_buscat.h
  *@brief generated protocol source code
  *@author make_protocol.py
  *@date 08/11/20
  */

#include "buscatService.h"
#include "Platforms/Common/mrt_platform.h"


/**
  *@brief Initialize the packet service
  */
void app_buscat_init();

/**
  *@brief ends service
  */
void app_buscat_end();

/**
  *@brief process the data for the packet service
  */
void app_buscat_process();