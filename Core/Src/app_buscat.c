/**
  *@file app_buscat.c
  *@brief generated protocol source code
  *@author make_protocol.py
  *@date 08/11/20
  */

/***********************************************************
        Application Layer
***********************************************************/

#include "main.h"
#include "app_buscat.h"
#include "usbd_cdc_if.h"

static uint8_t iface0_rx_buf[512];
static char printBuf[512];

static inline HandlerStatus_e iface0_write(uint8_t* data, int len)
{
  /* Place code for writing bytes on interface 0 here */
  CDC_Transmit_FS(data, len);

  return PACKET_SENT;
}


/*******************************************************************************
  App Init/end
*******************************************************************************/
void app_buscat_init()
{

  //initialize service
  bc_service_init(1,16);

  bc_service_register_bytes_tx(0, iface0_write);

}

void app_buscat_end()
{
}

/*******************************************************************************
  App Process
*******************************************************************************/

void app_buscat_process()
{
  /* process the actual service */
  bc_service_process();

}


/*******************************************************************************
  Packet handlers
*******************************************************************************/
/**
  *@brief Handler for receiving SpiTransfer packets
  *@param SpiTransfer incoming SpiTransfer packet
  *@return handling bc_status
  */
HandlerStatus_e bc_SpiTransfer_handler(bc_packet_t* bc_SpiTransfer, bc_packet_t* bc_Data)
{
  /*  Get Required Fields in packet */
  uint8_t data[32];  //array of data
  uint8_t rxBuf[32];

  int len = bc_getData(bc_SpiTransfer, data);

  MRT_SPI_TRANSFER(&hspi1, data, rxBuf, len, 1000);

  bc_setData(bc_Data, rxBuf, len);


  return PACKET_HANDLED;
}

/**
  *@brief Handler for receiving GpioWrite packets
  *@param GpioWrite incoming GpioWrite packet
  *@return handling bc_status
  */
HandlerStatus_e bc_GpioWrite_handler(bc_packet_t* bc_GpioWrite)
{
  /*  Get Required Fields in packet */
  uint8_t port;  //selects port for IO operation
  uint8_t pin;  //pin number for IO
  uint8_t value;  //

  port = bc_getPort(bc_GpioWrite);
  pin = bc_getPin(bc_GpioWrite);
  value = bc_getValue(bc_GpioWrite);


  mrt_gpio_t gpio;
  gpio.pin =  0x00000001 << pin;


  switch(port)
  {
    case BC_PORT_PORT_A:    // port A
      gpio.port = GPIOA;
      break;
    case BC_PORT_PORT_B:    // port B
      gpio.port = GPIOB;
      break;
    case BC_PORT_PORT_C:    // port C
      gpio.port = GPIOC;
      break;
    case BC_PORT_PORT_D:    // port D
      gpio.port = GPIOD;
      break;
    default:
      break;
  }

  MRT_GPIO_WRITE(gpio, value);




  return PACKET_HANDLED;
}

/**
  *@brief Handler for receiving I2CRegWrite packets
  *@param I2CRegWrite incoming I2CRegWrite packet
  *@return handling bc_status
  */
HandlerStatus_e bc_I2CRegWrite_handler(bc_packet_t* bc_I2CRegWrite)
{
  /*  Get Required Fields in packet */
  uint8_t devAddr;  //device address for i2c device
  uint16_t regAddr;  //register address
  uint8_t regAddrLen;  //length of reg address for register operation
  uint8_t data[32];  //array of data

  devAddr = bc_getDevAddr(bc_I2CRegWrite);
  regAddr = bc_getRegAddr(bc_I2CRegWrite);
  regAddrLen = bc_getRegAddrLen(bc_I2CRegWrite);
  bc_getData(bc_I2CRegWrite, data);




  return PACKET_HANDLED;
}


/**
  *@brief catch-all handler for any packet not handled by its default handler
  *@param metaPacket ptr to bc_packet_t containing packet
  *@param bc_response ptr to response
  *@return handling bc_status
  */
HandlerStatus_e bc_default_handler( bc_packet_t * bc_packet, bc_packet_t * bc_response)
{


  return PACKET_HANDLED;
}
